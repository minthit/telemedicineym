﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using TeleMedicineYM.Models;

namespace TeleMedicineYM.Hubs
{
    public class TeleMedHub: Hub
    {
        public static List<MemberRoomModel> memberList = new List<MemberRoomModel>();

        public async Task Signal(string toId, string message)
        {
            await Clients.Client(toId).SendAsync("Signal", GetConnectionId(), message);
        }


        public override async Task OnConnectedAsync()
        {
            var httpContext = Context.GetHttpContext();
            var roomName = httpContext.Request.Query["roomName"];

            await JoinRoom(roomName);

            MemberRoomModel connectedMember = new MemberRoomModel()
            {
                roomName = roomName,
                socketId = GetConnectionId()
            };
            memberList.Add(connectedMember);

            int clientCount = memberList.Where(m => m.roomName.Equals(roomName)).Count();
            var socketListByRoom = memberList.Where(m => m.roomName.Equals(roomName)).Select(m => m.socketId).ToList();

            await Clients.Group(roomName).SendAsync("UserJoined", GetConnectionId(), clientCount, socketListByRoom);

            await base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception exception)
        {
            var member = memberList.Where(m => m.socketId.Equals(GetConnectionId())).FirstOrDefault();

            memberList.Remove(member);

            int clientCount = memberList.Where(m => m.roomName.Equals(member.roomName)).Count();
            await Clients.Group(member.roomName).SendAsync("UserLeft", GetConnectionId(), clientCount);

            await LeaveRoom(member.roomName);

            await base.OnDisconnectedAsync(exception);
        }

        //Helpers
        public string GetConnectionId()
        {
            return Context.ConnectionId;
        }

        public async Task JoinRoom(string roomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
        }

        public async Task LeaveRoom(string roomName)
        {
            await Groups.AddToGroupAsync(Context.ConnectionId, roomName);
        }

    }
}
